{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "from sklearn.datasets import fetch_20newsgroups\n",
    "from sklearn.feature_extraction.text import CountVectorizer\n",
    "from sklearn.cluster import KMeans\n",
    "from sklearn import metrics\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "This week we will look at working with text as data, how to extract features from text and the use of a clustering algorithm.\n",
    "\n",
    "We will take some samples of texts and look at how to extract a fixed set of features from each text to use in clustering.   We'll then look at how to measure the similarity or distance between two texts. Finally we'll look at the KMeans clustering algorithm.\n",
    "\n",
    "New concepts this week:\n",
    "- using **feature extraction** methods to create features from texts\n",
    "- **sparse arrays** are used to store arrays where many of the values will be zero\n",
    "- comparing the similarity of two samples using a **distance metric**\n",
    "- the **kmeans clustering** algorithm\n",
    "\n",
    "## Finding Text Data\n",
    "\n",
    "The example this week is derived from [this example](http://scikit-learn.org/stable/auto_examples/text/document_clustering.html#sphx-glr-auto-examples-text-document-clustering-py) in the sklearn documentation.\n",
    "\n",
    "We will use some data from sklearn, this is the [20 newsgroups dataset](http://scikit-learn.org/stable/datasets/twenty_newsgroups.html#newsgroups) containing messages from the old Usenet discussion boards.   We select just four of the groups giving us messages on four topics.  We choose two that are probably quite close together (atheism and religion) and two that should be quite different.\n",
    "\n",
    "The result is an sklearn dataset, the actual data is available as dataset.data, the newsgroup names are in dataset.target."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load some categories from the training set\n",
    "categories = [\n",
    "    'alt.atheism',\n",
    "    'talk.religion.misc',\n",
    "    'comp.graphics',\n",
    "    'sci.space',\n",
    "]\n",
    "\n",
    "dataset = fetch_20newsgroups(subset='all', categories=categories,\n",
    "                             shuffle=True, random_state=42)\n",
    "len(dataset.target)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we can look at the first message in the data\n",
    "print(dataset.data[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Feature Extraction\n",
    "\n",
    "We can't work directly with text as data - we need some kind of numerical or categorical features to use in the algorithms we're working with.  Text has a variable number of words per sample, we need a fixed set of features.\n",
    "\n",
    "A very common feature type for text treats each sample as a *bag of words* and just records how often each word is present in the text.  Each word becomes a feature, the value is the count of how many times it occurs in the sample.  Of course, there will be thousands of words in general, so we just choose the N most frequent words as features.  \n",
    "\n",
    "The idea is that if a particular word occurs a lot in two texts, they might be similar. If the same pattern \n",
    "of words is frequent in both, even more so.  However, some words are very frequent in all texts - and, of, \n",
    "is etc - they don't tell you much about what the text is saying; it is common to remove these common \n",
    "words, generally known as *stop words*, before you do any feature extraction.\n",
    "\n",
    "SKLearn has a collection of [text feature extraction](http://scikit-learn.org/stable/modules/feature_extraction.html#text-feature-extraction) methods that we can make use of. We'll use the simplest of these, [CountVectorizer](http://scikit-learn.org/stable/modules/feature_extraction.html#common-vectorizer-usage) which just counts the number of times a word occurs in the text.  We pass it a parameter that defines the maximum number of features (words) to use and the name of the stop word list.  \n",
    "\n",
    "Once we've made a vectorizer, we can use the *fit_transform* method to apply it to a set of data. \n",
    "In this first example we will just compute 10 features, just to make it easier to look at the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "count_vec = CountVectorizer(max_features=10, stop_words='english')\n",
    "X = count_vec.fit_transform(dataset.data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result *X_count* is a SciPy [sparse matrix](https://docs.scipy.org/doc/scipy-0.18.1/reference/sparse.html).  \n",
    "Many of the feature values will be zero if the given word does not occur in the text. To store all of these\n",
    "zeros would be very wasteful of memory, so we use a *sparse matrix* which uses methods to only store\n",
    "the data that is non-zero.  The SciPy sparse matrix classes support some of the matrix methods that you can use\n",
    "on regular Numpy arrays or Pandas dataframes, but not all.  \n",
    "\n",
    "In the example below we use the *getrow* method to get a single row and the *toarray* method to convert this to a regular numpy array.  \n",
    "\n",
    "First, we can look at the words that have been selected as features via the *feature_names* method on the vectorizer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "count_vec.get_feature_names()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we only chose 10 features so they aren't likely to be very good at characterising the texts.  You might\n",
    "also notice that we have 'words' like *com* and *edu*, probably from email addresses and *don* which is\n",
    "probably from *don't*.  The question of what is a word is not a simple one.\n",
    "\n",
    "## Measuring Similarity\n",
    "\n",
    "We now have a fixed size feature set for each text - the frequency of ten words.  We can look at the features\n",
    "that have been computed for the first text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X.getrow(0).toarray()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "this means that the word *article* appears twice in the text, *edu* appears six times and *com* and *god* do not\n",
    "appear at all.  \n",
    "\n",
    "If we want to measure the **similarity** of this text with another, we can compare their feature vectors.  If we\n",
    "were dealing with points on a plane in a geometry problem, we could work out the **distance** between\n",
    "the points using Pythagoras Theorem. Two points that were very close could be said to be very similar.  This is \n",
    "known as the **Euclidean distance** metric and in fact, we can use it for this problem too. \n",
    "\n",
    "The Euclidean distance is defined as the square root of the sum of the squares of the differences between each \n",
    "pair of feature values:\n",
    "\n",
    "\\begin{equation*}\n",
    "distance = \\sqrt{\\sum_{i=1}^n (a_i - b_i)^2}\n",
    "\\end{equation*}\n",
    "\n",
    "Here's an example of computing the distance between the first two rows of the dataset.  I've done it explicitly\n",
    "with raw vector arithmetic and then using the SciPy *euclidean* function as a check:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a1 = count_vec.transform([dataset.data[0]]).toarray()[0]\n",
    "a2 = X.getrow(1).toarray()[0]\n",
    "\n",
    "# import the scipy euclidean function as a check\n",
    "from scipy.spatial.distance import euclidean\n",
    "\n",
    "d1= np.sqrt((np.square(a1-a2)).sum())\n",
    "d2= euclidean(a1, a2) \n",
    "print(\"Distance between articles 0 and 1:\", d1, d2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this distance isn't a physical distance in metres,m it has no units, we just know that if it \n",
    "is bigger, the articles are more different in their feature sets.\n",
    "\n",
    "We can use this to look through the data and find the most similar article to a given target text.  The function \n",
    "I've written below calculates the euclidean distance between a given target article and every other article\n",
    "in the dataset.  It remembers the article with the smallest distance and returns it's index.\n",
    "\n",
    "I've tested this using the vectorizer I made above (*count_vec*) to find the closest article\n",
    "to the first one in the datast (note that I've passed dataset.data[:1] to the function so that I \n",
    "don't just find the first article). The result is not very similar - we're only using 10 word features\n",
    "after all."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_closest(dataset, target, vectorizer):\n",
    "    \"\"\"Find the most similar article in dataset to target using \n",
    "    the given vectorizer to extract feature vectors\n",
    "    Returns the index of the most similar article\"\"\"\n",
    "    \n",
    "    a1 = vectorizer.transform([target]).toarray()[0]\n",
    "    X = vectorizer.transform(dataset)\n",
    "    \n",
    "    best = 0\n",
    "    best_dist = 9999\n",
    "    for i in range(X.shape[0]):\n",
    "        a2 = X.getrow(i).toarray()[0]\n",
    "        dist = euclidean(a1, a2)\n",
    "        if dist < best_dist:\n",
    "            best_dist = dist\n",
    "            best = i\n",
    "    return best\n",
    "\n",
    "best = find_closest(dataset.data[1:], dataset.data[0], count_vec)\n",
    "\n",
    "print(\"Closest article is \", best)\n",
    "print(dataset.data[1:][best])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Checkpoint** Repeat the analysis I did above but use a larger number of features - say 200.  Do you get a\n",
    "result that is more similar to the target article? (Hint: there is another article that directly quotes\n",
    "this one, that should be very similar)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# make a new vectoriser with more features and repeat the analysis...\n",
    "\n",
    "count_vec = CountVectorizer(max_features=10, stop_words='english')\n",
    "X = count_vec.fit_transform(dataset.data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## KMeans Clustering\n",
    "\n",
    "Finally we look at the [KMeans clustering algorithm](http://scikit-learn.org/stable/modules/clustering.html#k-means).  This makes use of the distance metric like the one\n",
    "we've used above.  KMeans tries to find a given number of clusters in the data. It does this by grouping\n",
    "together the samples that are closest to one another using the distance metric.\n",
    "\n",
    "KMeans starts by choosing K points (K is the number of clusters) somewhere in the space. These\n",
    "are the initial cluster centres. It then assigns\n",
    "each sample to one cluster based on which cluster centre it is closest too.   Once all points are\n",
    "in a cluster, the cluster centre is re-computed and the process is repeated.  This continues until\n",
    "there is no (or little) change to the centroids or until some maximum number of iterations.  \n",
    "\n",
    "In this example we ask the algorithm to look for 4 clusters in our data, the verbose flag will \n",
    "show the number of iterations as it runs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "km = KMeans(n_clusters=4, init='k-means++', max_iter=100, n_init=1, verbose=True)\n",
    "km.fit(X_count)\n",
    "labels = dataset.target"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*km* is now the result of clustering, *km.labels_* are the labels assigned to each sample, they are just numbers 0..3 since the algorithm doesn't know what the true labels were.   \n",
    "\n",
    "This is an example of an **Unsupervised Learning** algorithm.  We didn't tell it what the true answer\n",
    "was, we just asked it to look for a given number of clusters in the data.  \n",
    "\n",
    "To evaluate the result we can use the [SKLearn metrics](http://scikit-learn.org/stable/modules/clustering.html#homogeneity-completeness-and-v-measure) module.  Here we compute:\n",
    "\n",
    "- homogeneity -- larger if each cluster contains members of a single class\n",
    "- completeness -- larger if all samples from a single class are in the same cluster\n",
    "- v-measure -- is the harmonic mean of the homogeneity and completeness\n",
    "\n",
    "Ideally, these metrics would be close to 1.0\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Homogeneity: %0.3f\" % metrics.homogeneity_score(labels, km.labels_))\n",
    "print(\"Completeness: %0.3f\" % metrics.completeness_score(labels, km.labels_))\n",
    "print(\"V-measure: %0.3f\" % metrics.v_measure_score(labels, km.labels_))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extension\n",
    "\n",
    "As an extension exercise, repeat the KMeans clustering exercise but use an alternate feature vector. \n",
    "The [TfidfVectorizer](http://scikit-learn.org/stable/modules/feature_extraction.html#tfidf-term-weighting) (from sklearn.feature_extraction.text import TfidfVectorizer) uses a measure \n",
    "tf-idf that tries to measure how characteristic a word is in a text.  Words that are usually infrequent\n",
    "but occur many times in a text will have a higher score.   Use a much higer number of features (say 1000) and \n",
    "see if you can get a better set of evaluation scores than in the example above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
